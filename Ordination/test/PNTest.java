package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;

public class PNTest {
	
	private PN PNtest1;
	private PN PNtest2;
	private PN PNtest3;
	private PN PNtest4;
	private PN PNtest5;
	private Laegemiddel drugs;
	
	@Before
	public void setUp() throws Exception {
		drugs = new Laegemiddel("drugs", 3, 5, 8, "pille");
	}
	
	@Test
	public void testPN() {
		PNtest1 = new PN(LocalDate.of(2017, 03, 06),LocalDate.of(2017, 03, 10),drugs, 2);
		assertEquals(LocalDate.of(2017, 03, 06), PNtest1.getStartDen());
		assertEquals(LocalDate.of(2017, 03, 10), PNtest1.getSlutDen());
		assertEquals(drugs, PNtest1.getLægemiddel());
		assertEquals(2, PNtest1.getAntalEnheder(),0.0001);
	}
	
	@Test
	public void testGivDosis() {
		PNtest1 = new PN(LocalDate.of(2017, 03, 06),LocalDate.of(2017, 03, 10),drugs, 2);
		assertTrue(PNtest1.givDosis(LocalDate.of(2017, 03, 07)));
		assertTrue(PNtest1.givDosis(LocalDate.of(2017, 03, 9)));
		assertFalse(PNtest1.givDosis(LocalDate.of(2017, 03, 04)));
		assertFalse(PNtest1.givDosis(LocalDate.of(2017, 03, 12)));
	}
	
	@Test
	public void testSamletDosis() {
		PNtest1 = new PN(LocalDate.of(2017, 03, 06),LocalDate.of(2017, 03, 10),drugs, 10);
		PNtest1.givDosis(LocalDate.of(2017, 03, 07));
		PNtest1.givDosis(LocalDate.of(2017, 03, 07));
		PNtest1.givDosis(LocalDate.of(2017, 03, 8));
		assertEquals(30, PNtest1.samletDosis(),0.0001);
		
		PNtest2 = new PN(LocalDate.of(2017, 03, 06),LocalDate.of(2017, 03, 10),drugs, 5);
		PNtest2.givDosis(LocalDate.of(2017, 03, 07));
		PNtest2.givDosis(LocalDate.of(2017, 03, 07));
		assertEquals(10, PNtest2.samletDosis(), 0.0001);
		
		PNtest3 = new PN(LocalDate.of(2017, 03, 06),LocalDate.of(2017, 03, 10),drugs, 4);
		PNtest3.givDosis(LocalDate.of(2017, 03, 07));
		PNtest3.givDosis(LocalDate.of(2017, 03, 07));
		PNtest3.givDosis(LocalDate.of(2017, 03, 07));
		assertEquals(12, PNtest3.samletDosis(),0.0001);
		
		PNtest4 = new PN(LocalDate.of(2017, 03, 06),LocalDate.of(2017, 03, 10),drugs, 6);
		PNtest4.givDosis(LocalDate.of(2017, 03, 07));
		assertEquals(6, PNtest4.samletDosis(), 0.0001);
		
		PNtest5 = new PN(LocalDate.of(2017, 03, 06),LocalDate.of(2017, 03, 10),drugs, 2);
		PNtest5.givDosis(LocalDate.of(2017, 03, 07));
		PNtest5.givDosis(LocalDate.of(2017, 03, 07));
		PNtest5.givDosis(LocalDate.of(2017, 03, 07));
		PNtest5.givDosis(LocalDate.of(2017, 03, 07));
		PNtest5.givDosis(LocalDate.of(2017, 03, 07));
		PNtest5.givDosis(LocalDate.of(2017, 03, 07));
		PNtest5.givDosis(LocalDate.of(2017, 03, 07));
		PNtest5.givDosis(LocalDate.of(2017, 03, 07));
		PNtest5.givDosis(LocalDate.of(2017, 03, 07));
		assertEquals(18, PNtest5.samletDosis(),0.0001);
	}

	
	@Test
	public void testDoegnDosis() {
		PNtest1 = new PN(LocalDate.of(2017, 03, 06),LocalDate.of(2017, 03, 11),drugs, 2);
		PNtest1.givDosis(LocalDate.of(2017, 03, 07));
		PNtest1.givDosis(LocalDate.of(2017, 03, 8));
		PNtest1.givDosis(LocalDate.of(2017, 03, 9));
		PNtest1.givDosis(LocalDate.of(2017, 03, 10));
		PNtest1.givDosis(LocalDate.of(2017, 03, 11));
		assertEquals(2, PNtest1.doegnDosis(), 0.0001);
		
		PNtest2 = new PN(LocalDate.of(2017, 03, 06),LocalDate.of(2017, 03, 10),drugs, 1);
		PNtest2.givDosis(LocalDate.of(2017, 03, 07));
		PNtest2.givDosis(LocalDate.of(2017, 03, 8));
		PNtest2.givDosis(LocalDate.of(2017, 03, 9));
		PNtest2.givDosis(LocalDate.of(2017, 03, 10));
		assertEquals(1, PNtest2.doegnDosis(), 0.0001);
		
		PNtest3 = new PN(LocalDate.of(2017, 03, 06),LocalDate.of(2017, 03, 10),drugs, 5);
		PNtest3.givDosis(LocalDate.of(2017, 03, 07));
		PNtest3.givDosis(LocalDate.of(2017, 03, 8));
		PNtest3.givDosis(LocalDate.of(2017, 03, 9));
		PNtest3.givDosis(LocalDate.of(2017, 03, 10));
		assertEquals(5, PNtest3.doegnDosis(), 0.0001);

	}
	
}
