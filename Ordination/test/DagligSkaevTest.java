package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;

public class DagligSkaevTest {

	private Laegemiddel drugs;
	private DagligSkaev dagSkaev1;
	private DagligSkaev dagSkaev2;
	private DagligSkaev dagSkaev3;

	
	@Before
	public void setUp() throws Exception {
		drugs = new Laegemiddel("THC", 0.5, 1, 1.75, "drops");
	}
	
	@Test
	public void testDagligSkaev() {
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] antal = {2 , 5, 8, 4};
		dagSkaev1 = new DagligSkaev(LocalDate.of(2017, 03, 07), LocalDate.of(2017, 03, 9), drugs, kl, antal);
		assertEquals(LocalDate.of(2017, 03, 07), dagSkaev1.getStartDen());
		assertEquals(LocalDate.of(2017, 03, 9), dagSkaev1.getSlutDen());
		assertEquals(drugs, dagSkaev1.getLægemiddel());
		
		assertEquals(4, kl.length);
		assertEquals(LocalTime.of(12, 0), kl[0]);
		assertEquals(LocalTime.of(12, 40), kl[1]);
		assertEquals(LocalTime.of(16, 0), kl[2]);
		assertEquals(LocalTime.of(18, 45), kl[3]);

		assertEquals(4, antal.length);
		assertEquals(2, antal[0], 0.0001);
		assertEquals(5, antal[1], 0.0001);
		assertEquals(8, antal[2], 0.0001);
		assertEquals(4, antal[3], 0.0001);


	}
	

	@Test
	public void testDoegnDosis() {
		LocalTime[] kl1 = {LocalTime.of(20, 15)};
		double[] antal1 = {24};
		dagSkaev1 = new DagligSkaev(LocalDate.of(2017, 03, 06), LocalDate.of(2017, 03, 11), drugs, kl1, antal1);
		assertEquals(4, dagSkaev1.doegnDosis(), 0.0001);
		
		LocalTime[] kl2 = {LocalTime.of(20, 15), LocalTime.of(14, 30)};
		double[] antal2 = {24, 6};
		dagSkaev2 = new DagligSkaev(LocalDate.of(2017, 03, 06), LocalDate.of(2017, 03, 11), drugs, kl2, antal2);
		assertEquals(5, dagSkaev2.doegnDosis(), 0.0001);
		
		LocalTime[] kl3 = {LocalTime.of(20, 15), LocalTime.of(14, 30), LocalTime.of(8, 00)};
		double[] antal3 = {24, 6, 6};
		dagSkaev3 = new DagligSkaev(LocalDate.of(2017, 03, 06), LocalDate.of(2017, 03, 11), drugs, kl3, antal3);
		assertEquals(6, dagSkaev3.doegnDosis(), 0.0001);

	}

	
	@Test
	public void testSamletDosis() {
		LocalTime[] kl1 = {LocalTime.of(20, 15)};
		double[] antal1 = {24};
		dagSkaev1 = new DagligSkaev(LocalDate.of(2017, 03, 06), LocalDate.of(2017, 03, 10), drugs, kl1, antal1);
		assertEquals(120, dagSkaev1.samletDosis(), 0.0001);
		
		LocalTime[] kl2 = {LocalTime.of(20, 15), LocalTime.of(14, 30)};
		double[] antal2 = {24, 6};
		dagSkaev2 = new DagligSkaev(LocalDate.of(2017, 03, 06), LocalDate.of(2017, 03, 12), drugs, kl2, antal2);
		assertEquals(210, dagSkaev2.samletDosis(),0.0001);
		
		LocalTime[] kl3 = {LocalTime.of(20, 15), LocalTime.of(14, 30), LocalTime.of(8, 00)};
		double[] antal3 = {24, 6, 6};
		dagSkaev3 = new DagligSkaev(LocalDate.of(2017, 03, 06), LocalDate.of(2017, 03, 16), drugs, kl3, antal3);
		assertEquals(396, dagSkaev3.samletDosis(), 0.0001);
	}

	@Test
	public void testOpretDosis() {
		LocalTime[] kl1 = {LocalTime.of(20, 15)};
		double[] antal1 = {3};
		dagSkaev1 = new DagligSkaev(LocalDate.of(2017, 03, 06), LocalDate.of(2017, 03, 10), drugs, kl1, antal1);
		assertEquals(1, dagSkaev1.getDoser().size());
		dagSkaev1.opretDosis(LocalTime.of(20, 30), 3);
		assertEquals(2, dagSkaev1.getDoser().size());
		
		dagSkaev1.opretDosis(null, 5);
		assertEquals(2, dagSkaev1.getDoser().size());
		
		dagSkaev1.opretDosis(LocalTime.of(15, 25), -1);
		assertEquals(2, dagSkaev1.getDoser().size());
	}
	

}
