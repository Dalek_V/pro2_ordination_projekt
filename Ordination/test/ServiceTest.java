package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class ServiceTest
{

	@Before
	public void setUp() throws Exception
	{
		
	}

	@Test
	public void getService()
	{
		assert(Service.getService().equals(Service.getService()));
	}
	
	@Test
	public void testOpretPNOrdination()
	{
		Service service = Service.getTestService();
		service.createSomeObjects();
		Patient patient = service.getAllPatienter().get(0);
		Laegemiddel laegemiddel = service.getAllLaegemidler().get(0);
		int antal = 1;

		try
		{
			service.opretPNOrdination(LocalDate.of(2000, 1, 5), LocalDate.of(2000, 1, 8), patient, laegemiddel, antal);
		}
		catch (IllegalArgumentException e)
		{
			fail("Exception detected, PN Case 1");
		}

		try
		{
			service.opretPNOrdination(LocalDate.of(2000, 1, 5), LocalDate.of(2000, 1, 4), patient, laegemiddel, antal);
			fail("Exception not Detected, PN case 2");
		}
		catch (IllegalArgumentException e)
		{
		}
		try
		{
			service.opretPNOrdination(LocalDate.of(2000, 1, 5), LocalDate.of(2000, 1, 8), patient, laegemiddel, antal);
		}
		catch (IllegalArgumentException e)
		{
			fail("Exception detected, PN case 3");
		}
		try
		{
			service.opretPNOrdination(LocalDate.of(2000, 12, 30), LocalDate.of(2001, 1, 1), patient, laegemiddel, antal);
		}
		catch (IllegalArgumentException e)
		{
			fail("Exception Detected, PN case 4");
		}
		try
		{
			service.opretPNOrdination(LocalDate.of(2001, 1, 1), LocalDate.of(2000, 12, 30), patient, laegemiddel, antal);
			fail("Exception not detected, PN case 5");
		}
		catch (IllegalArgumentException e)
		{
		}
	}

	@Test
	public void testOpretDagligFastOrdination()
	{
		Service service = Service.getTestService();
		service.createSomeObjects();
		Patient patient = service.getAllPatienter().get(0);
		Laegemiddel laegemiddel = service.getAllLaegemidler().get(0);
		int morgen = 1;
		int middag = 2;
		int aften = 3;
		int nat = 4;
		
		try
		{
			service.opretDagligFastOrdination(LocalDate.of(2000, 1, 5), LocalDate.of(2000, 1, 8), patient, laegemiddel, morgen, middag, aften, nat);
		}
		catch (IllegalArgumentException e)
		{
			fail("Exception detected, Daglig Case 1");
		}

		try
		{
			service.opretDagligFastOrdination(LocalDate.of(2000, 1, 5), LocalDate.of(2000, 1, 4), patient, laegemiddel, morgen, middag, aften, nat);
			fail("Exception not Detected, Daglig case 2");
		}
		catch (IllegalArgumentException e)
		{
		}
		try
		{
			service.opretDagligFastOrdination(LocalDate.of(2000, 1, 5), LocalDate.of(2000, 1, 8), patient, laegemiddel, morgen, middag, aften, nat);
		}
		catch (IllegalArgumentException e)
		{
			fail("Exception detected, Daglig case 3");
		}
		try
		{
			service.opretDagligFastOrdination(LocalDate.of(2000, 12, 30), LocalDate.of(2001, 1, 1), patient, laegemiddel, morgen, middag, aften, nat);
		}
		catch (IllegalArgumentException e)
		{
			fail("Exception Detected, Daglig case 4");
		}
		try
		{
			service.opretDagligFastOrdination(LocalDate.of(2001, 1, 1), LocalDate.of(2000, 12, 30), patient, laegemiddel, morgen, middag, aften, nat);
			fail("Exception not detected, Daglig case 5");
		}
		catch (IllegalArgumentException e)
		{
		}
	}
	
	@Test
	public void testOpretDagligSkaevOrdination()
	{
		Service service = Service.getTestService();
		service.createSomeObjects();
		Patient patient = service.getAllPatienter().get(0);
		Laegemiddel laegemiddel = service.getAllLaegemidler().get(0);
		LocalTime[] klokkeslet = new LocalTime[3];
		double[] antal = new double[3];
		for (int i = 1; i < 4; i++)
		{
			antal[i-1] = i;
		}
		klokkeslet[0] = LocalTime.of(1, 1);
		klokkeslet[1] = LocalTime.of(1, 2);
		klokkeslet[2] = LocalTime.of(1, 3);
		try
		{
			service.opretDagligSkaevOrdination(LocalDate.of(2000, 1, 5), LocalDate.of(2000, 1, 8), patient, laegemiddel, klokkeslet, antal);
		}
		catch (IllegalArgumentException e)
		{
			fail("Exception detected, Skaev Case 1");
		}

		try
		{
			service.opretDagligSkaevOrdination(LocalDate.of(2000, 1, 5), LocalDate.of(2000, 1, 4), patient, laegemiddel, klokkeslet, antal);
			fail("Exception not Detected, Skaev case 2");
		}
		catch (IllegalArgumentException e)
		{
		}
		try
		{
			service.opretDagligSkaevOrdination(LocalDate.of(2000, 1, 5), LocalDate.of(2000, 1, 8), patient, laegemiddel, klokkeslet, antal);
		}
		catch (IllegalArgumentException e)
		{
			fail("Exception detected, Skaev case 3");
		}
		try
		{
			service.opretDagligSkaevOrdination(LocalDate.of(2000, 12, 30), LocalDate.of(2001, 1, 1), patient, laegemiddel, klokkeslet, antal);
		}
		catch (IllegalArgumentException e)
		{
			fail("Exception Detected, Skaev case 4");
		}
		try
		{
			service.opretDagligSkaevOrdination(LocalDate.of(2001, 1, 1), LocalDate.of(2000, 12, 30), patient, laegemiddel, klokkeslet, antal);
			fail("Exception not detected, Skaev case 5");
		}
		catch (IllegalArgumentException e)
		{
		}
		
		// Array Længder test.
		double[] antalFail = new double[1];
		antalFail[0] = 1;
		LocalTime[] klokkeFail = new LocalTime[1];
		klokkeFail[0] = LocalTime.of(1, 1);
		
		try
		{
			service.opretDagligSkaevOrdination(LocalDate.of(2000, 12, 30), LocalDate.of(2001, 1, 1), patient, laegemiddel, klokkeFail, antalFail);
		}
		catch (IllegalArgumentException e)
		{
			fail("Exception Detected, Skaev case 6");
		}
		
		
		try
		{
			service.opretDagligSkaevOrdination(LocalDate.of(2000, 12, 30), LocalDate.of(2001, 1, 1), patient, laegemiddel, klokkeFail, antal);
			fail("Exception not Detected, Skaev case 7");
		}
		catch (IllegalArgumentException e)
		{
		}
		
	}
	
	@Test
	public void testOrdinationPNAnvendt()
	{
		Service service = Service.getTestService();
		service.createSomeObjects();
		PN pn = new PN(LocalDate.of(2000, 1, 5), LocalDate.of(2000, 1, 10), service.getAllLaegemidler().get(0), 1);
		
		try
		{
			service.ordinationPNAnvendt(pn, LocalDate.of(2000, 1, 5));
		}
		catch (IllegalArgumentException e)
		{
			fail("Exception detected, PNAnvendt case 1");
		}
		try
		{
			service.ordinationPNAnvendt(pn, LocalDate.of(2000, 1, 10));
		}
		catch (IllegalArgumentException e)
		{
			fail("Exception detected, PNAnvendt case 2");
		}
		try
		{
			service.ordinationPNAnvendt(pn, LocalDate.of(2000, 1, 8));
		}
		catch (IllegalArgumentException e)
		{
			fail("Exception detected, PNAnvendt case 3");
		}
		try
		{
			service.ordinationPNAnvendt(pn, LocalDate.of(2000, 1, 4));
			fail("Exception not detected, PNAnvendt case 4");
		}
		catch (IllegalArgumentException e)
		{
		}
		try
		{
			service.ordinationPNAnvendt(pn, LocalDate.of(2000, 1, 11));
			fail("Exception not detected, PNAnvendt case 5");
		}
		catch (IllegalArgumentException e)
		{
		}
	}

	@Test
	public void testAnbefaletDosisPrDoegn()
	{
		Service service = Service.getTestService();
		service.createSomeObjects();
		Laegemiddel testmiddel = new Laegemiddel("Testesterone", 1.5, 2, 5, "");
		Patient patient1 = new Patient("", "", 10);
		Patient patient2 = new Patient("", "", 24);
		Patient patient3 = new Patient("", "", 25);
		Patient patient4 = new Patient("", "", 120);
		Patient patient5 = new Patient("", "", 121);
		
		assert(service.anbefaletDosisPrDoegn(patient1, testmiddel) == 15);
		assert(service.anbefaletDosisPrDoegn(patient2, testmiddel) == 36);
		assert(service.anbefaletDosisPrDoegn(patient3, testmiddel) == 50);
		assert(service.anbefaletDosisPrDoegn(patient4, testmiddel) == 240);
		assert(service.anbefaletDosisPrDoegn(patient5, testmiddel) == 605);
		
	}
	
	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel()
	{
		Service service = Service.getTestService();
		service.createSomeObjects();
		assert(service.antalOrdinationerPrVægtPrLægemiddel(24, 80, service.getAllLaegemidler().get(2)) == 1);
		assert(service.antalOrdinationerPrVægtPrLægemiddel(40, 125, service.getAllLaegemidler().get(1)) == 3);
	}
}
