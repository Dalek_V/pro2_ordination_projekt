package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;

public class DagligFastTest {

    private DagligFast dagligFast;
    private Laegemiddel laegemiddel;

    @Before
    public void setUp() throws Exception {
        laegemiddel = new Laegemiddel("Antelopehår", 0.250, 1.4, 2.5, "Sprøjte");
        dagligFast = new DagligFast(LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 5), laegemiddel,
            4, 3, 0, 2);
    }

    @Test
    public void testDagligFast() {
        DagligFast dagligFastObject =
            new DagligFast(LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 5), laegemiddel,
                4, 3, 0, 2);
        assertEquals(4, dagligFastObject.getDoser()[0].getAntal(), 0.00001);
        assertEquals(3, dagligFastObject.getDoser()[1].getAntal(), 0.00001);
        assertEquals(0, dagligFastObject.getDoser()[2].getAntal(), 0.00001);
        assertEquals(2, dagligFastObject.getDoser()[3].getAntal(), 0.00001);
        assertEquals(laegemiddel, dagligFastObject.getLægemiddel());
        assertEquals(LocalDate.of(2017, 1, 1), dagligFastObject.getStartDen());
        assertEquals(LocalDate.of(2017, 1, 5), dagligFastObject.getSlutDen());
    }

    @Test
    public void testSamletDosis() {
        dagligFast = new DagligFast(LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 5), laegemiddel,
            5, 0, 0, 0);
        assertEquals(25, dagligFast.samletDosis(), 0.0001);
        
        dagligFast =
            new DagligFast(LocalDate.of(2017, 1, 5), LocalDate.of(2017, 1, 15), laegemiddel,
                4, 2, 3, 0);
        assertEquals(99, dagligFast.samletDosis(), 0.0001);
        
        dagligFast = new DagligFast(LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 2), laegemiddel,
            4, 4, 4, 4);
        assertEquals(32, dagligFast.samletDosis(), 0.0001);
        
        dagligFast = new DagligFast(LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 5), laegemiddel,
            0, 0, 0, 0);
        assertEquals(0, dagligFast.samletDosis(), 0.0001);
        
        dagligFast = new DagligFast(LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 7), laegemiddel,
            0, 0, 2, 3);
        assertEquals(35, dagligFast.samletDosis(), 0.0001);
    }

    @Test
    public void testDoegnDosis() {
        dagligFast = new DagligFast(LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 5), laegemiddel,
            5, 0, 0, 0);
        assertEquals(5, dagligFast.doegnDosis(), 0.0001);
        
        dagligFast = new DagligFast(LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 5), laegemiddel,
            4, 2, 3, 0);
        assertEquals(9, dagligFast.doegnDosis(), 0.0001);
        
        dagligFast = new DagligFast(LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 5), laegemiddel,
            4, 4, 4, 4);
        assertEquals(16, dagligFast.doegnDosis(), 0.0001);
        
        dagligFast = new DagligFast(LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 5), laegemiddel,
            0, 0, 0, 0);
        assertEquals(0, dagligFast.doegnDosis(), 0.0001);
        
        dagligFast = new DagligFast(LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 5), laegemiddel,
            0, 0, 2, 3);
        assertEquals(5, dagligFast.doegnDosis(), 0.0001);
    }
}
