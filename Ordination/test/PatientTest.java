package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;

public class PatientTest {

    private Patient patient;
    private Ordination pn;
    private Laegemiddel laegemiddel;
    
    @Before
    public void setUp() throws Exception {
        patient = new Patient("1234567890", "Hans Hansen", 88.8);
        laegemiddel = new Laegemiddel("Antelopehår", 0.250, 1.4, 2.5, "Sprøjte");
        pn = new PN(LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 5), laegemiddel, 5);
    }

    @Test
    public void testPatient() {
        Patient patientObject = new Patient("1234567890", "Jens Jensen", 88.8);
        assertNotNull(patientObject);
        assertEquals("1234567890", patientObject.getCprnr());
        assertEquals("Jens Jensen", patientObject.getNavn());
        assertEquals(88.8, patientObject.getVaegt(), 0.00001);

    }

    @Test
    public void testSetNavn() {
        assertEquals("Hans Hansen", patient.getNavn());
        patient.setNavn("Hans Hans Hansen");
        assertEquals("Hans Hans Hansen", patient.getNavn());
    }

    @Test
    public void testSetVaegt() {
        assertEquals(88.8, patient.getVaegt(), 0.00001);
        patient.setVaegt(90.2);
        assertEquals(90.2, patient.getVaegt(), 0.00001);
    }

    @Test
    public void testAddOrdination() {
        assertEquals(0, patient.getOrdinationer().size());
        patient.addOrdination(pn);
        assertEquals(1, patient.getOrdinationer().size());
        assertTrue(patient.getOrdinationer().contains(pn));
    }

    @Test
    public void testRemoveOrdination() {
        patient.addOrdination(pn);
        assertTrue(patient.getOrdinationer().contains(pn));
        patient.removeOrdination(pn);
        assertFalse(patient.getOrdinationer().contains(pn));
    }
}
