package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Ordination;
import ordination.PN;
import service.Service;

public class OrdinationTest
{
	Service service;
	@Before
	public void setUp() throws Exception
	{
		service = Service.getTestService();
		service.createSomeObjects();
	}

	@Test
	public void testAntalDage()
	{
		Ordination test1 = new PN(LocalDate.of(2000, 1, 5), LocalDate.of(2000, 1, 8), service.getAllLaegemidler().get(0), 0);
		assert(test1.antalDage() == 4);
		
		Ordination test2 = new PN(LocalDate.of(2000, 12, 30), LocalDate.of(2001, 1, 1), service.getAllLaegemidler().get(0), 0);
		assert(test2.antalDage() == 3);
		
		Ordination test3 = new PN(LocalDate.of(2000, 1, 5), LocalDate.of(2000, 1, 5), service.getAllLaegemidler().get(0), 0);
		assert(test3.antalDage() == 1);
	}
}
