package ordination;

import java.time.*;

public class DagligFast extends Ordination {
    
    Dosis[] doser;
    
    public DagligFast(LocalDate startDen,
        LocalDate slutDen, Laegemiddel laegemiddel, double morgenAntal, double middagAntal,
        double aftenAntal, double natAntal) {
        super(startDen, slutDen, laegemiddel);

        doser = new Dosis[4];
        
        doser[0] = new Dosis(LocalTime.of(06, 00), morgenAntal);
        doser[1] = new Dosis(LocalTime.of(12, 00), middagAntal);
        doser[2] = new Dosis(LocalTime.of(18, 00), aftenAntal);
        doser[3] = new Dosis(LocalTime.of(00, 00), natAntal);
    }
    
    @Override
    public double samletDosis() {
        return doegnDosis() * (super.antalDage());
    }
    
    @Override
    public double doegnDosis() {
        int count = 0;
        for (int i = 0; i < doser.length; i++) {
            count += doser[i].getAntal();
        }
        return count;
    }
    
    @Override
    public String getType() {
        return "DagligFast";
    }

    public Dosis[] getDoser() {
        return doser.clone();
    }
}
