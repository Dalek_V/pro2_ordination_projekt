package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;

public class PN extends Ordination {
    
    private double antalEnheder;
    private ArrayList<LocalDate> doseringsDatoer = new ArrayList<>();
    
    public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel lægemiddel, double antal) {
        super(startDen, slutDen, lægemiddel);
        this.antalEnheder = antal;
    }
    
    /**
     * Registrerer at der er givet en dosis paa dagen givesDen
     * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
     * Retrurner false ellers og datoen givesDen ignoreres
     * @param givesDen
     * @return
     */
    public boolean givDosis(LocalDate givesDen) {
        if (givesDen.isAfter(getSlutDen()) || givesDen.isBefore(getStartDen())) {
            return false;
        }
        else {
            doseringsDatoer.add(givesDen);
            Collections.sort(doseringsDatoer);
            return true;
        }
    }
    
    @Override
    public double doegnDosis() {
        // TODO

    	if (getAntalGangeGivet() > 0) {
			if (ChronoUnit.DAYS.between(doseringsDatoer.get(0), doseringsDatoer.get(doseringsDatoer.size()-1)) < 1) {
				return samletDosis();
			} else {
				return this.samletDosis() / (1+ChronoUnit.DAYS.between(doseringsDatoer.get(0), 
						doseringsDatoer.get(doseringsDatoer.size()-1)));
			}
		} else {
			return 0.0;
		}
    }
    
    @Override
    public double samletDosis() {
        return getAntalEnheder() * getAntalGangeGivet();
    }
    
    /**
     * Returnerer antal gange ordinationen er anvendt
     * @return
     */
    public int getAntalGangeGivet() {
        return this.doseringsDatoer.size();
    }
    
    public double getAntalEnheder() {
        return antalEnheder;
    }
    
    @Override
    public String getType() {
        return "PN";
    }
    
}
