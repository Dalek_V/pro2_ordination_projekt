package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination{
   
	 private ArrayList<Dosis> doser = new ArrayList<Dosis>();

	 public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, 
			 LocalTime[] klokkeslet, double[] antalEnheder) {
			super(startDen, slutDen, laegemiddel);
			
			for (int i = 0; i < antalEnheder.length; i++) {
				this.opretDosis(klokkeslet[i], antalEnheder[i]);
			}
		}

    public void opretDosis(LocalTime tid, double antal) {
    	if (tid != null && antal > 0) {
    		Dosis dose = new Dosis(tid, antal);
        	doser.add(dose);
		}
    }

	@Override
	public double samletDosis() {
		double days = super.antalDage();
		double count = 0.0;
		for (Dosis dosis : doser) 
		{
			count += dosis.getAntal();
		}
		return days * count;
	}

	@Override
	public double doegnDosis() {
		double doserPrDoegn = 0.0;
		for (Dosis dosis : doser)
		{
			doserPrDoegn += dosis.getAntal();
		}
		return doserPrDoegn / super.antalDage();
	}

	@Override
	public String getType() {

		return "DagligSkaev";
	}
	
	public ArrayList<Dosis> getDoser()
	{
		return new ArrayList<Dosis>(doser);
	}
}
